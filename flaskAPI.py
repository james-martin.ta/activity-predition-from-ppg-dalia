from flask import Flask
from flask_restplus import Resource, Api 
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import random
import json
import copy

app = Flask(__name__)
api = Api(app=app)

# Import des datas : 
df = pd.read_csv("./data/fullData.csv")
x = copy.copy(df)
# pré-proce
x.drop(x.columns[0], axis=1)
x['Gender'] = [(1 if x=="m" else x) for x in x['Gender']]
x['Gender'] = [(1 if x==" m" else x) for x in x['Gender']]
x['Gender'] = [(0 if x==" f" else x) for x in x['Gender']]
for i in range(1, 16) :
  x['Subject'] = [(i if x=="S"+str(i) else x) for x in x['Subject']]

Y = x["Activity"]
X = x.drop(["Activity"], axis=1)
xTrain, xTest, yTrain, yTest = train_test_split(X, Y, test_size = 0.2, random_state = 0)

# RandomForest
model = RandomForestClassifier()
model.fit(xTrain, yTrain)

@api.route("/randomForestWithRandomIndex")
class randomForestWithRandomIndex(Resource):
    def get(self):
        index = random.randrange(517955)
        random_y = x.iloc[[index]]["Activity"]
        random_x = x.iloc[[index]].drop(["Activity"], axis=1)

        predicted = model.predict(random_x)

        return json.dumps({
            "randomIndex" : index,
            "predicted" : predicted[0],
            "right_target" : random_y.values[0]
        })

#@api.route("/activity/<int:id>")
if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8000', debug=True)