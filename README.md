# Activity Predition from PPG-DaLiA 
* Data : https://archive.ics.uci.edu/ml/datasets/PPG-DaLiA
* Powerpoint : https://docs.google.com/presentation/d/12uKmVTlqOBKLXtzijHuYCdbmOtQ-_gnjroWiHlmZwfM/edit?usp=sharing 
---- 
## 1Preprocessing.ipynb -> Partie 1 : Import des datas et pré-processing 
* Import des données depuis : https://archive.ics.uci.edu/ml/datasets/PPG-DaLiA
* Traitement pour obtenir des CSV
* Export des data dans le directory ./data/
* Lien du Collab : https://colab.research.google.com/drive/1PiXM8GYEKnRXxuQPlpQ-5Izv1aeqNs5N
---- 
## 2DataVizualisation.ipynb -> Partie 2 : Data viz et exploration
* Import des données depuis : https://archive.ics.uci.edu/ml/datasets/PPG-DaLiA
* Lien du Collab : https://colab.research.google.com/drive/1HTzgmi3HSERZ8FqZ73yoxdegsIxKcL6j
----
## 3Models.ipynb -> Partie 3 : Models
* Lien du Collab : https://colab.research.google.com/drive/1ap-p2kq-qOyemNNyjLYJmBNftV7wOinO
* Le randomForest est le meilleur model réalisé
----
## API Flask PTI -> Partie 4 : API
* Aller dans le répertoire du projet
* Lancer la commande `python3 flaskAPI.py`
* Aller à l'URL *http://localhost:8000/*
* La fonction REST GET lance une prédiction sur une ligne du dataset globale ./data/fullData.csv